<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 11/16/2016
 * Time: 6:58 PM
 */

namespace App\BITM\SEIP143203\Birthday;
use App\BITM\SEIP143203\Message\Message;
use App\BITM\SEIP143203\Utility\Utility;
use App\BITM\SEIP143203\Model\Database as DB;
use PDO;



class Birthday extends DB
{
    public $id="";
    public $name="";
    public $birthday="";



    public function __construct()
    {
        parent::__construct();
    }



    public function setData($data=null)
    {
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('birthday',$data)){
            $this->birthday=$data['birthday'];

        }
    }

    public function index($fetchMode='ASSOC')
    {
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from birthday Where is_deleted='0'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);

        $all_birthday=$sth->fetchAll();


        return  $all_birthday;
    }

// end of index();



    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from birthday Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_person=$sth->fetch();

        return  $selected_person;

    }


    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->birthday);

        //var_dump($values);


        $query='UPDATE birthday  SET  name = ?   , birthday = ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ name: $this->name ] ,
               [ Birthday: $this->birthday ] <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from birthday Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }




    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->birthday);
        $query="insert into birthday(name,birthday) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Name: $this->name ] ,
               [ birthday: $this->birthday ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');



    }
    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE birthday  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }



}
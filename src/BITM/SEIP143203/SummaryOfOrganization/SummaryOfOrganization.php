<?php
namespace App\BITM\SEIP143203\SummaryOfOrganization;
use App\BITM\SEIP143203\Message\Message;
use App\BITM\SEIP143203\Utility\Utility;
use App\BITM\SEIP143203\Model\Database as DB;
use PDO;

class SummaryOfOrganization extends DB
{
    public $id="";
    public $organization_name="";
    public $organization_summary="";



    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }


    public function setData($data=null)
    {

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->organization_name=$data['organization_name'];

        }
        if(array_key_exists('organization_summary',$data)){
            $this->organization_summary=$data['organization_summary'];

        }
    }

    public function store(){
        $dbh=$this->connection;
        $values=array($this->organization_name,$this->organization_summary);
        $query="insert into summary_of_organization(organization_name,organization_summary) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
            Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
 [ organization_summary: $this->organization_summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else         Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
 [ organization_summary: $this->organization_summary ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');



    }



    public function index($fetchMode='ASSOC')
    {
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from summary_of_organization where is_deleted='0'");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_organization=$sth->fetchAll();


            return  $all_organization;
        }







    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from summary_of_organization Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_organization=$sth->fetch();

        return  $selected_organization;

    }




    public function update(){
        $dbh=$this->connection;
     $values=array($this->organization_name,$this->organization_summary);

        //var_dump($values);


        /*$query='UPDATE summary_of_organization  SET organization_name  = ?   , organization_summary = ? where id ='.$this->id;*/
        $query='UPDATE summary_of_organization SET organization_name= ? , organization_summary= ?  WHERE id = '.$this->id ;
        $sth=$dbh->prepare($query);
        $sth->execute($values);
       /* var_dump($values);die;*/
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Organization name: $this->organization_name ] ,
               [ Organization Summary: $this->organization_summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");

        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from summary_of_organization Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }


    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE summary_of_organization  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }



}


<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143203\Message\Message;
if(!isset( $_SESSION)) session_start();
$message1=Message::message();



use App\BITM\SEIP143203\Gender\Gender;

$obj= new Gender();


$all_person= $obj->index();

?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="../../../resource/Bootstrap/font-awesome/css/font-awesome.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/general3.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >

<!-- Top menu -->
<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="">Bootstrap Registration Form Template</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="top-navbar-1">
            <ul class="nav navbar-nav navbar-right">
                <li style="color: #fff;">
							<span class="li-text ">
								You can connect to here...
							</span>
                   <!-- <a href="#"><strong>links</strong></a>-->

                    <span class="li-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-envelope"></i></a>
								<a href="#"><i class="fa fa-skype"></i></a>
							</span>
                </li>
            </ul>
        </div>
    </div>
</nav>






<div class="container ">

    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> Gender List</h1>


                </div>
            </div>





            <div class="panel-body">
                <div class="table-responsive" >
                    </br></br></br></br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gender</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial=0;

                            foreach($all_person as $result){
                            $serial++; ?>
                            <td><?php echo $serial?></td>
                            <td><?php echo $result['id']?></td>
                            <td><?php echo $result['name']?></td>
                            <td><?php echo $result['gender']?></td>
                            <td><a href="view.php?id=<?php echo $result['id']  ?>" class="btn btn-primary" role="button">View</a>
                                <a href="edit.php?id=<?php echo $result['id'] ?>"  class="btn btn-info" role="button">Edit</a>
                                <a href="delete.php?id=<?php echo $result['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                                <a href="trash.php?id=<?php echo $result['id'] ?>"  class="btn btn-info" role="button">Trash</a>
                            </td>

                        </tr>
                        <?php }?>
                        <a href="trashed.php"   class="btn btn-info role="button"> View Trashed List</a> &nbsp;&nbsp;&nbsp;
                        <a href="create.php "  class="btn btn-info role="button"> Add new person</a>



                        </tbody>
                    </table>
                    <div id="confirmation_message" style="color:red;">
                        <?php echo $message1 ?>
                    </div>

                </div>

            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });
</script>


<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>




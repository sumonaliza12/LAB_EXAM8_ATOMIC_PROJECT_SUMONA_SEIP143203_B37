<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143203\Utility\Utility;
use App\BITM\SEIP143203\Gender\Gender;


$obj= new Gender();
$id=$_GET['id'];



$selected_person= $obj->view($id);
//Utility::dd($selected_person);

?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/gender.css" type="text/css">

    <link rel="stylesheet" href="../../../resource/Bootstrap/font-awesome/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>

<body>

<!-- Top menu -->
<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="">ATOMIC PROJECT:GENDER</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="top-navbar-1">
            <ul class="nav navbar-nav navbar-right">
                <li style="color: #fff;">

                    <span class="li-text">
                           You can connect here too:
							</span>
                    <span class="li-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-envelope"></i></a>
								<a href="#"><i class="fa fa-skype"></i></a>
							</span>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update Gender Form</h1>


                    </div>




                </div>
                <div class="panel-body">


                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_person['name']?>" name="name" type="text">
                            <label class="h3">Select Gender:</label>
                            <div class="radio">
                                <label><input type="radio" name="gender" value="male" <?php if($selected_person['gender']=="male"):?>checked<?php endif ?>>male</label>
                            </div>
                            <div class="radio ">
                                <label><input type="radio" name="gender" value="female"<?php if($selected_person['gender']=="female"):?>checked<?php endif ?>>female</label>
                            </div>
                            <div class="radio ">
                                <label><input type="radio" name="gender" value="other"<?php if($selected_person['gender']=="other"):?>checked<?php endif ?>>other</label>
                            </div>
                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Add »">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>
<?php
require_once("../../../vendor/autoload.php");



use App\BITM\SEIP143203\Birthday\Birthday;
use App\BITM\SEIP143203\Message\Message;


if(!isset( $_SESSION)) session_start();
$message1=Message::message();

$obj= new Birthday();


$all_person= $obj->index();
$sorted_person=$all_person;


?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/birthday-109a.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }


</style>

<body  >
<div class="container ">
    <div style="margin-top: 40px ;float: right;"> <a href="../index.php" class="btn btn-info btn-danger btn-lg" role="button">Atomic Project List</a></br></br></div>


    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> Birthday List</h1>


                </div>
            </div>





            <div class="panel-body">
                <div class="table-responsive" >
                    </br></br></br></br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>&nbsp;&nbsp;Birthday</th>

                            <th> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial=0;

                            foreach($all_person as $result)?>
                            <script>
                            $( function() {
                                $( "#datepicker_<?php echo $result['id']?>" ).datepicker();
                            } );
                            </script>
                            <?php
                            $serial++; ?>
                            <td><?php echo $serial?></td>
                            <td><?php echo $result['id']?></td>
                            <td><?php echo $result['name']?></td>
                            <td><?php echo $result['birthday']?></td>
                            <td><a href="view.php?id=<?php echo $result['id']  ?>" class="btn btn-primary" role="button">View</a>
                                <a href="edit.php?id=<?php echo $result['id'] ?>"  class="btn btn-info" role="button">Edit</a>
                                <a href="delete.php?id=<?php echo $result['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                                <a href="trash.php?id=<?php echo $result['id'] ?>"  class="btn btn-info" role="button">Trash</a>
                            </td>

                        </tr>


                        <?php }?>
                        <a href="upcoming_birthdays.php" class="btn btn-primary" role="button">View upcoming birthdays</a> &nbsp;&nbsp;&nbsp;
                        <a href="trashed.php"   class="btn btn-info role="button"> View Trashed List</a> &nbsp;&nbsp;&nbsp;
                        <a href="create.php "  class="btn btn-info role="button"> Add new Birthday</a>
                        </br> </br> </br>


                        </tbody>
                    </table>
                    <div id="confirmation_message" style="color:red;">
                        <?php echo $message1 ?>
                    </div>


                </div>

            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });
</script>



<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>




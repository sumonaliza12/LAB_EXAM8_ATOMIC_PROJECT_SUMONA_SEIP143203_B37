<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143203\Message\Message;
if(!isset( $_SESSION)) session_start();
$message=Message::message();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BirthDay</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/birthday.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
</script>
    <!--datepicker-->

    <script language="JavaScript" src="../../../resource/Bootstrap/css/htmlDatePicker/htmlDatePicker.js" type="text/javascript"></script>
    <link href="../../../resource/Bootstrap/css/htmlDatePicker/htmlDatePicker.css" rel="stylesheet" />


</head>

<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Add Birthday</h1>
                        <div class="row-fluid user-row">
                            <img src="../../../resource/assets/images/cake.jpg" class="img-responsive icon" alt="Conxole Admin"/>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="store.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" placeholder="Enter Your Name Here" name="name" type="text">

                            <input type="date" id="datepicker" name="date" placeholder="Enter birth date..." class="form-date form-control"  readonly onclick="GetDate(this);" >

                            </br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Click To Insert »">
                        </fieldset>
                    </form>


                    <div id="confirmation_message">
                        <?php echo $message;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(5000).fadeOut();
        });
    });
</script>
</body>
</html>
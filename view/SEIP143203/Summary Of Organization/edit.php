<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP143203\Message\Message;


$obj= new \App\BITM\SEIP143203\SummaryOfOrganization\SummaryOfOrganization;
$id=$_GET['id'];

$obj->setData($_GET);



$selected_organization= $obj->view($id);
var_dump($selected_organization);

?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summary Of Organization</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/summary_of_organization.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h2>Update Organization summary</h2>

                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-summary" method="Post" action="update.php">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_organization['organization_name']?>"  name="organization_name" type="text">


                            <textarea type="text" name="organization_summary" class="form-control" id="title"  value=""><?php echo $selected_organization['organization_summary']?></textarea>
                            </br>
                            <input type="hidden" name="id" value="<?php echo $id?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
    </html><?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/13/2016
 * Time: 5:49 PM
 */